use std::fs;
use rss::Channel;

fn main() {
    println!("Hello, world!");

    let bytes = fs::read("podcast.xml").unwrap();

    let res = Channel::read_from(&bytes[..]).unwrap();

    for i in res.items {
        println!("iTunes extension present: {:?}", i.itunes_ext);
        println!("iTunes extension map {:?}", i.extensions.get("itunes").unwrap().get("image"));
    }
}
